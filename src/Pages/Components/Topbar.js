//topbar

import React, {Component} from 'react';
import {Navbar, Nav,NavDropdown, Form, FormControl, Button} from 'react-bootstrap';

import './Topbar.css'
import img from './logo.png'

class Topbar extends Component {
  render() {
    return (
      <div className = "OuterDiv">
        <div>
        <a class="active" href="/">Who Needs A Quote!?</a>
        </div>
        <div class = "topnavcenter">

        <a href="/SportsPage">Sports</a>
        <a href="/BusinessPage">Business</a>
        <a href="/PoliticsPage">Politics</a>
        </div>
      </div>

    )
  }
}

export default Topbar;
